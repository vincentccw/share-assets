const exec = require('child_process').exec;
const args = process.argv.slice(2);

const command = args[0];
const path = args[1] ? args[1].replace(/\\/gi, '/') : '';

// console.log("command: ", command);
// console.log("path: ", path);

const CYGWIN_BASH_PATH = 'C:/cygwin64/bin/bash.exe';
const REPO_PATH = 'repo'; // just 'repo' because the file is in the same dir as cygwin bash

console.log('command = ', command);
console.log('path = ', path);

function executeRepo (command, path) {
    return new Promise((resolve, reject) => {
        const myShellScript = exec(`${CYGWIN_BASH_PATH} -l ${REPO_PATH} ${command} -f "${path}"`);
        
        myShellScript.stdout.on('data', (data) => {
            // console.log('stdout data = ', data);
            resolve(data);
        });
        myShellScript.stderr.on('data', (data) => {
            // console.error('Bash error ', data);
            reject(data);
        });
    }).catch(error =>{
        console.log(error);
    });
}

if (path) executeRepo(command, path);

module.exports = {
    executeRepo,
}